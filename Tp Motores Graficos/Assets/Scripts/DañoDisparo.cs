using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoDisparo : MonoBehaviour
{
    public int vidaEnemigo = 100;
    public int dañoBala = 10;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bala")
        {

            vidaEnemigo -= dañoBala;

            if (vidaEnemigo <= 0)
            {
                Destroy(gameObject);
            }


        }
    }


}
