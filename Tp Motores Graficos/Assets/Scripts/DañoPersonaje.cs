using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoPersonaje : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Personaje")
        {
            VidaPersonaje vida = FindObjectOfType<VidaPersonaje>();

            vida.addnum();
        }
    }
}
