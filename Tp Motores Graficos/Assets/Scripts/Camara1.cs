using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara1 : MonoBehaviour
{
    private Transform camara;
    
    public Vector2 sensibilidad;

    void Start()
    {

        camara = transform.Find("Main Camera");
        
        Cursor.lockState = CursorLockMode.Locked;
    }


    void Update()
    {
        float hor = Input.GetAxis("Mouse X");
        float ver = Input.GetAxis("Mouse Y");
        if (hor != 0)
        {
            transform.Rotate(Vector3.up * hor * sensibilidad.x);
        }

        if (ver != 0)
        {
            
            float angulo = (camara. localEulerAngles.x - ver * sensibilidad.y + 360) % 360;
            if (angulo > 180) { angulo -= 360; }

            angulo = Mathf.Clamp(angulo, -80, 80);

            camara.localEulerAngles = Vector3.right * angulo;

        }

    }

}