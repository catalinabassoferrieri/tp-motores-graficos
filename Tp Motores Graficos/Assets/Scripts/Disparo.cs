using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{

    public GameObject UbicacionEsf;

    public GameObject EsfPrefab;

    public float VelocidadEsf;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            GameObject EsfTemp = Instantiate(EsfPrefab, UbicacionEsf.transform.position, UbicacionEsf.transform.rotation) as GameObject;

            Rigidbody rb = EsfTemp.GetComponent<Rigidbody>();

            rb.AddForce(transform.forward * VelocidadEsf);

            Destroy(EsfTemp, 5.0f);
        }
    }
}