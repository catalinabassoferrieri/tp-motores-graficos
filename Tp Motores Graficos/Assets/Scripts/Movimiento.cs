using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public float Velocidad;
    public float Salto = 10f; 
    public float MaxAltura = 2f; 
    private bool TocarSuelo = true; 
    void Start()
    {
        
    }

   
    void Update()
    {
        float Horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");
        Vector3 mov = new Vector3(Horizontal, 0.0f, Vertical);
        transform.Translate(mov * Time.deltaTime * Velocidad);

        if (TocarSuelo && Input.GetKeyDown(KeyCode.Space))
        {
           
            GetComponent<Rigidbody>().AddForce(Vector3.up * Salto, ForceMode.Impulse);
            TocarSuelo = false;
        }
    }
    void OnCollisionEnter(Collision collision)
    {
       
        if (collision.gameObject.CompareTag("Suelo"))
        {
            TocarSuelo = true;
        }
    }

   


}
