using UnityEngine;
using TMPro;


public class Colisiones : MonoBehaviour
{
    public TMP_Text Gol;
    public int score = 0;

    void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("Pelota"))
        {
           
            score++;
            Debug.Log("ĄGOOOOOLLLL! Puntaje actual: " + score);
            Gol.text = score.ToString();
        }
    }
}