using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


public class VidaPersonaje : MonoBehaviour
{
    public TMP_Text Vida;
    public int vidaPersonaje = 100;
    public int daņoPersonaje = 10;


    public void addnum()
    {

        vidaPersonaje -= daņoPersonaje;
        Vida.text = vidaPersonaje.ToString();

        if (vidaPersonaje <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }
}
