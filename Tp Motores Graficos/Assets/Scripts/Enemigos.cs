using UnityEngine;

public class Enemigos : MonoBehaviour
{
    public float speed = 3f; 
    public Transform target; 

    void Update()
    {
        
        if (target != null)
        {
            
            Vector3 direction = (target.position - transform.position).normalized;

           
            transform.Translate(direction * speed * Time.deltaTime);
        }
    }


}