using UnityEngine;

public class PatearPelota : MonoBehaviour
{
    public float fuerzaPateo = 10f; 
    public Rigidbody pelotaRB; 

    void Start()
    {
        
        if (pelotaRB == null)
        {
            pelotaRB = GetComponent<Rigidbody>();
        }
    }

    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
           
            pelotaRB.AddForce(transform.forward * fuerzaPateo, ForceMode.Impulse);
        }
    }
}